//
//  ViewController.swift
//  Binar-iOS-Networking
//
//  Created by Arie May Wibowo on 22/09/21.
//

import UIKit

struct Response: Codable {
    let id: Int
    let name: String
    let city: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userCity: UILabel!
    
    let session = URLSession.shared
    let baseUrl = "https://my-json-server.typicode.com/ariemay/restapi-demo"
    let userEndpoint = "/users"
    let postEndpoint = "/posts"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCall(baseurl: baseUrl, endpoint: userEndpoint)
        
    }
    
    func apiCall(baseurl: String, endpoint: String) {
        let task = session.dataTask(with: URL(string: baseurl + endpoint)!) { data, response, error in
            do {
                let jsonDecoder = JSONDecoder()
                let decodedResponse = try jsonDecoder.decode([Response].self, from: data!)
                print(decodedResponse[0].name) // get first user in list
                
                DispatchQueue.main.async {
                    self.userName.text = "Welcome \(decodedResponse[0].name)"
                    self.userCity.text = "You live in \(decodedResponse[0].city)"
                }
                
            } catch {
                print(error)
            }

        }
        task.resume()
    }


}

